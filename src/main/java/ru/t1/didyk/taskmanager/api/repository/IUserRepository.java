package ru.t1.didyk.taskmanager.api.repository;

import ru.t1.didyk.taskmanager.model.User;

public interface IUserRepository extends IRepository<User> {

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExists(String login);

    Boolean isEmailExists(String email);

}
