package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

}
