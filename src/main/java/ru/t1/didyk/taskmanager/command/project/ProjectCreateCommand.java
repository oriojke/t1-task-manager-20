package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(userId, name, description);
    }

}
