package ru.t1.didyk.taskmanager.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }
}
