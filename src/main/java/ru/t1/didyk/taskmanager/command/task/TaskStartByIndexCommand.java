package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
