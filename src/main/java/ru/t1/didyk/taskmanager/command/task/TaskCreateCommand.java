package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(userId, name, description);
    }

}
