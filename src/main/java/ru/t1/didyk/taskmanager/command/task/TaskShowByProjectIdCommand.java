package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Show tasks by project id.";
    }

    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

}
