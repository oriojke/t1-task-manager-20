package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Update project by id.";
    }

    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateById(userId, id, name, description);
    }

}
