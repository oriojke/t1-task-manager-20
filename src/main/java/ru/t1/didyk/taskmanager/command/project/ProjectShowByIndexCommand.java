package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}
