package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Show project by id.";
    }

    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
