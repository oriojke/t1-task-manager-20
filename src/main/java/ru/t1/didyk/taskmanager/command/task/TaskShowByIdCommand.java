package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Show task by id.";
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}
