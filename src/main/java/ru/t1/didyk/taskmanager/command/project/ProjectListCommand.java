package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Display all projects.";
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        int index = 1;
        final List<Project> projects = getProjectService().findAll(userId, sort);
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + "." + project);
            index++;
        }
    }

}
