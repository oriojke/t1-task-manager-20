package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Remove task by id.";
    }

    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(userId, id);
    }

}
