package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Unbind task to project.";
    }

    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

}
