package ru.t1.didyk.taskmanager.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported.");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command \"" + command + "\" is not supported.");
    }

}
