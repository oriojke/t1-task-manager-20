package ru.t1.didyk.taskmanager.exception.field;

public final class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException() {
        super("Error! Email exists.");
    }

}
