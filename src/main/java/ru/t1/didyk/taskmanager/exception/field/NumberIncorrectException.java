package ru.t1.didyk.taskmanager.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Incorrect number.");
    }

    public NumberIncorrectException(final String value, Throwable cause) {
        super("Error! Value \"" + value + "\" is incorrect.");
    }

}
